export interface IMessageBase {
  type: EMESSAGE_TYPE;
  value: unknown;
}

export enum EMESSAGE_TYPE {
  start,
  stop,
  text,
}

export interface IMessageStart extends IMessageBase {
  type: EMESSAGE_TYPE.start;
  value: null;
}

export interface IMessageStop extends IMessageBase {
  type: EMESSAGE_TYPE.stop;
  value: null;
}

export interface IMessageText extends IMessageBase {
  type: EMESSAGE_TYPE.text;
  value: string;
}

export type IMessage = IMessageStart | IMessageStop | IMessageText;
