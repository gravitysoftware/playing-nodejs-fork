import { EMESSAGE_TYPE, IMessage } from "./types";

const pingMio = () => {
  let i = 0;
  return setInterval(() => {
    i = i % 60 ? i + 1 : 1;
    process.send({ type: EMESSAGE_TYPE.text, value: i });
  }, 1000);
};

let s: NodeJS.Timer;

process.on("message", (message: IMessage) => {
  switch (message.type) {
    case EMESSAGE_TYPE.stop:
      if (s) {
        clearInterval(s);
      }
      process.send({type: EMESSAGE_TYPE.stop});
      break;
    case EMESSAGE_TYPE.text:
      console.log("From Local: ", message.value);
      break;
    case EMESSAGE_TYPE.start:
      s = pingMio();
      break;
  }
});

console.log("MIO: module loaded");