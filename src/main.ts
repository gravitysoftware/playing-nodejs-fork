import { ChildProcess, fork } from 'child_process';
import path from "path";
import { EMESSAGE_TYPE, IMessage } from "./types";

const pingLocal = () => {
  let i = 0;
  const ping = setInterval(() => {
    i = i % 60 ? i + 1 : 1;
    console.log("Local: ", i);
  }, 1_000);
  setTimeout(() => clearInterval(ping), 15_000);
};

const loadMio = (): ChildProcess => {
  console.log("Local: Loading MIO");
  const mio = fork(path.join(__dirname, "mio"));

  mio.on("message", (message: IMessage) => {
    switch (message.type) {
      case EMESSAGE_TYPE.stop:
        mio.kill();
        break;
      case EMESSAGE_TYPE.text:
        console.log('From mio: ', message.value);
        break;
    }
  });

  setTimeout(() => {
    console.log("Local: starting mio");
    mio.send({ type: EMESSAGE_TYPE.start });
  }, 1_000);

  setTimeout(() => {
    console.log("Local: stopping mio");
    mio.send({ type: EMESSAGE_TYPE.stop });
  }, 10_000);
  return mio;
};

const main = () => {
  pingLocal();
  setTimeout(() => {
    loadMio();
  }, 1_000);
};

main();
